from ressource import Ressource

class Ouvrier :
    """Permet d'instancier un ouvrier
    Attributs d'instance
    --------------------

    __laProduction : Dictionnaire <Ressource, int>
        ressources produites par l'ouvrier
        La clé correspond à une Ressource et la valeur est de type int et correspond à la quantité de ressource produite
        Si une ressource n'existe pas en tant que clé du dictionnaire, c'est que l'ouvrier ne la produit pas.
    """

    def __init__(self, desRessources: dict):
        """
        Constructeur
        :param desRessources: la production de ressources
        """
        self.setLaProduction(desRessources)

    def setLaProduction(self, value : dict)-> None:
        self.__laProduction = value

    def cout(self) -> int:
        """
        Retourne le cout de l'ouvrier qui vaut 0 pour un simple ouvrier
        :return:
        """
        return 0

    def quantiteByRessource(self, uneRessource : Ressource) -> int:
        """
        Permet de récupérer la quantité que produit l'ouvrier pour la ressource passée en paramètre
        :param uneRessource:
        :return:
        """
        qte = 0
        if uneRessource in self.__laProduction.keys() :
            qte = self.__laProduction[uneRessource]
        return qte

