import unittest
from ressource import Ressource
from ouvrier import Ouvrier


class testOuvrierCout(unittest.TestCase):

    def setUp(self) -> None:

        self.resBois = Ressource("bois")
        self.resPierre = Ressource("pierre")

        self.app1 = Ouvrier({self.resBois : 1, self.resPierre :1} )

    def testOuvrierCout(self):
        self.assertEqual(self.app1.cout(),0,"Erreur cout simple Ouvrier à zéro")


if __name__ == '__main__':
    unittest.main()
