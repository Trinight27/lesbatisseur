class Categorie :
    """Permet d'instancier une categorie
        Attributs d'instance
        --------------------
        __libelle : str
            libelle de la catégorie
        __salaire : int
            salaire associé à la catégorie
    """

    def __init__(self, unLibelle: str, unCout: int):
        self.setLibelle(unLibelle)
        self.setSalaire(unCout)

    def getSalaire(self) -> int:
        return self.__salaire

    def setSalaire(self,value: int) -> None:
        self.__salaire = value

    def setLibelle(self,value: str) -> None:
        self.__libelle = value