class Ressource :
    """Permet d'instancier une ressource
        Attributs d'instance
        --------------------
        __libelle : str
            libelle de la ressource
    """

    def __init__(self, unLibelle: str):
        self.setLibelle(unLibelle)

    def setLibelle(self,value: str):
        self.__libelle = value

    def getLibelle(self) -> str:
        return self.__libelle
